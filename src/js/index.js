import $ from 'jquery';
import popper from 'popper.js';
import bootstrap from 'bootstrap';
import "./jquery.nice-select.min.js";
import "./jquery-ui.min.js";

class Counter{
  constructor(){
    this.count = 1;
  }
  get(){
    return this.count;
  }
  inc(){
    return ++this.count;
  }
  dec(){
    return --this.count;
  }
  
}

let counter = new Counter();

$(document).ready(function() {
  $(document).on('click', 'a[href^="#"], .button_ask', function (event) {
    event.preventDefault();
    var length = $($.attr(this, 'href')).offset().top - $('header').height() + 1;
    $('html, body').animate({
        scrollTop: length
    }, 400 + Math.abs(length - $('body').scrollTop())/10, 'easeInOutQuart', function(){} );
  });

  $('select').niceSelect();
  $('.addParticipant').click(function(){
    $('#participantList').append($('#participant').html());
    $('.counter').last().text(counter.inc());
    if (counter.get() == 2){
      $('.removeParticipant').show();
    }
  })
  $('.removeParticipant').click(function(){
    $('#participantList .form-row').last().remove();
    if (counter.dec() == 1){
      $('.removeParticipant').hide();
    }
  })
});